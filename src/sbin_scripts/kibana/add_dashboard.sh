#!/usr/bin/env bash
dashboard_file="${1}"
kibana_url="${2:-http://localhost:5601}"
schema_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )/schema"

json validate \
  --schema-file="${schema_dir}/dashboard.json" \
  --document-file="${dashboard_file}" \
  && curl --request POST "${kibana_url}/api/kibana/dashboards/import" \
          --header "content-type: application/json" \
          --header "kbn-xsrf: true" \
          --data "@${dashboard_file}" \
          --silent \
          --show-error
