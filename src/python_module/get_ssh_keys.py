#!/usr/bin/env python
import os
import requests
import re
import sys


do_keys_api_url = 'https://api.digitalocean.com/v2/account/keys'
do_token_file = '/home/ssh_updater/do_token'
username_key = 'username'
public_key_key = 'public_key'
name_key = 'name'


def parse_username(obj, name_re):
    result = None
    if public_key_key in obj:
        re_match = re.search(name_re, obj[public_key_key])
        if re_match:
            result = re_match.group(0)
    return result


def user_reducer(acc, cv):
    username = cv[username_key]
    key = cv[public_key_key]
    if username not in acc:
        acc[username] = []
    acc[username].append(key)
    return acc


def download_keys(token, filter_re, name_re):
    """
    Query the Digital Ocean api to get the current list of authorized SSH keys
    Filter the list to only include keys that match the filter regex.
    FILTERING IS NOT A SECURITY MECHANISM. IT ONLY CHECKS THAT THE COMMENT ON THE KEY IS A VALID EMAIL
    Reduce the list to be a Dict with usernames as the keys and List of public keys as the value
    Return the reduced list
    """
    headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer %s' % token
    }
    r = requests.get(do_keys_api_url, headers=headers)
    # Filter out keys that don't match filter regex
    filtered_keys = filter(lambda obj: re.search(filter_re, obj[name_key]), r.json()['ssh_keys'])
    # Return new object of just parsed username and public key
    keys = map(lambda obj: {public_key_key: obj[public_key_key], username_key: parse_username(obj, name_re)}, filtered_keys)
    # Remove entries that don't have a valid username
    filtered_keys = filter(lambda obj: obj[username_key] is not None, keys)
    # Combine all keys for a username into a single object
    reduced_keys = reduce(user_reducer, filtered_keys, {})
    return reduced_keys


def get_do_token():
    """Read the Digital Ocean API token from disk"""
    with open(do_token_file, 'r') as token_file:
        return token_file.read().strip()


def main(key_dir, filter_re, name_re):
    do_token = get_do_token()
    users = download_keys(do_token, filter_re, name_re)
    for username, keys in users.iteritems():
        with open(os.path.join(key_dir, username), 'w') as key_file:
            key_file.write('\n'.join(keys))


if __name__ == '__main__':
    main(sys.argv[1], sys.argv[2], sys.argv[3])
