import argparse
from os import path

import requests

__version__ = '1.0.0'
REPORTS = [
    {
        'id': 26101,
        'type': 'vendor',
    },
    {
        'id': 26103,
        'type': 'spending',
    }
]
FISCAL_API_URL = 'https://fiscalca.opengov.com/api'
REPORT_URL = '%s/v1/reports/{0}' % FISCAL_API_URL
SCHEMA_URL = '%s/transactions/v2/schema/{0}' % FISCAL_API_URL
DATA_DIR = '/var/lib/fiscal'


def parse_args():
    parser = argparse.ArgumentParser(description='''
    Download the latest California FI$Cal data.
    ''')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s version ' + __version__)
    parser.add_argument(
        '-d',
        '--directory',
        default=DATA_DIR,
        help='The directory where data should be saved. Default is "{0}".'.format(DATA_DIR),
    )
    return parser.parse_args()


def get_file_url(report_id):
    transaction_id = requests.get(REPORT_URL.format(report_id)).json()['transaction_id']
    schema = requests.get(SCHEMA_URL.format(transaction_id)).json()['downloads']
    return filter(lambda d: d['fiscal_year'] is None, schema)[0]['file_url']


def download_csv(fh, url):
        response = requests.get(url, stream=True)
        with open(fh, "wb") as handle:
            for chunk in response.iter_content(chunk_size=1024):
                if chunk:  # filter out keep-alive new chunks
                    handle.write(chunk)


def get_report(report, directory):
    report_type = report['type']
    print '[LOAD FISCAL] Getting download url for "{0}" report data'.format(report_type)
    download_url = get_file_url(report['id'])
    print '[LOAD FISCAL] Downloading "{0}" report data'.format(report_type)
    download_csv(path.join(directory, 'fiscal_{0}.csv'.format(report_type)), download_url)


def main(directory = DATA_DIR):
    if not path.exists(directory):
        raise IOError('Directory does not exist')
    for report in REPORTS:
        get_report(report, directory)


if __name__ == '__main__':
    args = parse_args()
    main(args.directory)
