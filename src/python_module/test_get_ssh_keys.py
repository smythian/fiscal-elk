#!/usr/bin/env python

import json
import unittest
from os import remove, path
from shutil import rmtree
from tempfile import mkstemp, mkdtemp

from mock import patch
from pkg_resources import resource_string

import get_ssh_keys

keys_json = json.loads(resource_string(__name__, 'keys.json'))
keys = keys_json["ssh_keys"]
name_re = '([A-z0-9_.@-]*)@[A-z0-9.-]+[.][A-z]{2,}$'
filter_re = '^fiscal_'


def mocked_requests_get(*args, **kwargs):
    class MockResponse:
        def __init__(self, json_data, status_code):
            self.json_data = json_data
            self.status_code = status_code

        def json(self):
            return self.json_data

    return MockResponse(keys_json, 200)


class TestGetSSHKeys(unittest.TestCase):
    def test_parse_username(self):
        self.assertEqual(get_ssh_keys.parse_username(keys[0], name_re), 'fiscal-elk1@example.com')
        self.assertEqual(get_ssh_keys.parse_username(keys[1], name_re), 'fiscal-elk2@example.com')

    @patch('requests.get', side_effect=mocked_requests_get)
    def test_download_keys(self, mock_get):
        mock_get.return_value = keys_json
        self.assertEqual(get_ssh_keys.download_keys('Hello there', filter_re, name_re), {
            u'fiscal-elk1@example.com': [keys[0]['public_key']],
        })

    def test_get_do_token(self):
        fh, abs_path = mkstemp()
        old_token_file = get_ssh_keys.do_token_file
        try:
            get_ssh_keys.do_token_file = abs_path
            with open(abs_path, 'w') as test_file:
                test_file.write('   supercalifragilisticexpialidocious     ')
            self.assertEqual(get_ssh_keys.get_do_token(), 'supercalifragilisticexpialidocious')
        finally:
            get_ssh_keys.do_token_file = old_token_file
            remove(abs_path)

    @patch('requests.get', side_effect=mocked_requests_get)
    def test_main(self, _):
        abs_path = mkdtemp()
        old_token_file = get_ssh_keys.do_token_file
        try:
            get_ssh_keys.do_token_file = path.join(abs_path, 'do_token')
            with open(get_ssh_keys.do_token_file, 'w') as test_file:
                test_file.write('   supercalifragilisticexpialidocious     ')
            get_ssh_keys.main(abs_path, filter_re, name_re)
            self.assertTrue(path.exists(path.join(abs_path, 'fiscal-elk1@example.com')))
            with open(path.join(abs_path, 'fiscal-elk1@example.com'), 'r') as key_file:
                self.assertEqual(key_file.read(), keys[0]['public_key'])
        finally:
            get_ssh_keys.do_token_file = old_token_file
            rmtree(abs_path)
