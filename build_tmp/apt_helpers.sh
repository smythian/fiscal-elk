#!/usr/bin/env bash

export DEBIAN_FRONTEND=noninteractive
RETRIES=15

add_key() {
  wget -qO - "${1}" | apt-key add -
}

apt_get () {
  result=1
  echo "[INSTALL] apt-get ${@}"
  for ((i = 1; i <= RETRIES; i += 1)); do
    apt-get "$@" > /dev/null
    [[ "${?}" -eq 0 ]] && result=0 && break
    locker_line=`lslocks | grep apt | head -n 1`
    locker_name=`echo "${locker_line}" | awk '{printf $1}'`
    if [[ -z "${locker_name// }" ]]; then
      echo "[INSTALL] Killing ${locker_name} which has blocking lock"
      pkill "${locker_name}"
    fi
    echo "[INSTALL] Retry ${i}"
    sleep 1
  done
  return "${result}"
}

export -f add_key
export -f apt_get
