resource "digitalocean_firewall" "fiscal-elk" {
  name = "fiscal-elk-${terraform.workspace}"
  tags = ["fiscal", "Code4Sac", "ELK"]
  inbound_rule = [
    {
      protocol           = "tcp"
      port_range         = "22"
      source_addresses   = ["0.0.0.0/0", "::/0"]
    },
    {
      protocol           = "tcp"
      port_range         = "80"
      source_addresses   = ["0.0.0.0/0", "::/0"]
    },
    {
      protocol           = "tcp"
      port_range         = "443"
      source_addresses   = ["0.0.0.0/0", "::/0"]
    },
    {
      protocol           = "tcp"
      port_range         = "5044"
      source_addresses   = ["0.0.0.0/0", "::/0"]
    },
  ]

  outbound_rule = [
    {
      protocol                = "tcp"
      port_range              = "53"
      destination_addresses   = ["1.1.1.1", "8.8.8.8", "9.9.9.9", "208.67.222.222"]
    },
    {
      protocol                = "udp"
      port_range              = "53"
      destination_addresses   = ["1.1.1.1", "8.8.8.8", "9.9.9.9", "208.67.222.222"]
    },
    {
      protocol                = "tcp"
      port_range              = "80"
      destination_addresses   = ["0.0.0.0/0", "::/0"]
    },
    {
      protocol                = "tcp"
      port_range              = "443"
      destination_addresses   = ["0.0.0.0/0", "::/0"]
    },
    {
      protocol                = "udp"
      port_range              = "123"
      destination_addresses   = ["129.6.15.28", "129.6.15.29", "129.6.15.30", "129.6.15.27", "24.56.178.140", "132.163.97.2", "132.163.97.3", "132.163.97.4", "132.163.96.2", "132.163.96.3", "132.163.96.4"]
    },
  ]
}
