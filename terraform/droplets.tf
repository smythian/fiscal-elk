resource "digitalocean_droplet" "fiscal-elk" {
  image = "ubuntu-18-04-x64"
  name = "fiscal-elk-${terraform.workspace}"
  region = "sfo2"
  size = "${var.droplet_size}"
  backups = false
  monitoring = true
  ipv6 = false
  private_networking = false
  ssh_keys = ["${var.ci_ssh_fingerprint}"]
  tags = ["fiscal", "Code4Sac", "ELK"]

  connection {
    user = "root"
    type = "ssh"
    private_key = "${var.ci_pvt_key}"
    timeout = "2m"
  }

  provisioner "file" {
    source = "${path.module}/../build_tmp"
    destination = "/root/build"
  }

  provisioner "file" {
    source = "${path.module}/../Pipfile"
    destination = "/root/build/Pipfile"
  }

  provisioner "remote-exec" {
    inline = [
      "chmod 700 /root/build/*.sh",
      "/root/build/01_harden.sh || exit $?",
      "/root/build/02_install.sh || exit $?",
    ],
  }

  provisioner "file" {
    source = "${path.module}/../static/"
    destination = "/"
  }

  provisioner "file" {
    source = "${path.module}/../src/sbin_scripts"
    destination = "/usr/sbin/fiscal_elk"
  }

  provisioner "file" {
    source = "${path.module}/../src/bin_scripts"
    destination = "/usr/bin/fiscal_elk"
  }

  provisioner "file" {
    source = "${path.module}/../src/python_module"
    destination = "/usr/lib/python2.7/dist-packages/fiscal_elk"
  }

  provisioner "remote-exec" {
    inline = [
      "/root/build/03_configure.sh ${var.do_token} ${terraform.workspace == "default" ? "" : format("%s.", terraform.workspace)}${var.domain_name} ${var.lets_encrypt_email} || exit $?",
      "/root/build/04_populate_elk.sh || exit $?",
    ]
  }

  provisioner "local-exec" {
//    Execute reboot from local-exec so that the script exits successfully
    command = "echo \"$${SSH_KEY}\" | ssh-add - && ssh -o 'StrictHostKeyChecking no' root@\"$${SSH_IP}\" \"/root/build/05_finalize.sh\"",
    environment {
      SSH_KEY = "${var.ci_pvt_key}"
      SSH_IP = "${digitalocean_droplet.fiscal-elk.ipv4_address}"
    }
  }
}
