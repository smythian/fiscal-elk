variable "do_token" {
  type = "string"
  description = "The Digital Ocean API token"
}

variable "ci_pvt_key" {
  type = "string"
  description = "The Digital Ocean API token"
}

variable "ci_ssh_fingerprint" {
  type = "string"
  description = "The Digital Ocean API token"
}

variable "floating_ip" {
  type = "string"
  description = "The floating IP address to use for the instance"
}

variable "domain_name" {
  type = "string"
  description = "The server's fully qualified domain name"
}

variable "lets_encrypt_email" {
  type = "string"
  description = "Email to use for Let's Encrypt registration"
  default = ""
}

variable "droplet_size" {
  type = "string"
  description = "The size of DigitalOcean droplet to use (minimum s-2vcpu-4gb)"
  default = "s-2vcpu-4gb"
}
