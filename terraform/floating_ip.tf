resource "digitalocean_floating_ip" "fiscal-elk" {
  droplet_id = "${digitalocean_droplet.fiscal-elk.id}"
  region     = "${digitalocean_droplet.fiscal-elk.region}"
  ip_address = "${var.floating_ip}"
}
